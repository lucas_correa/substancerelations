#include <string>
#include "SubstancesRelation.h"

void automatedTest();

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
        std::string command = argv[1]; 
        if (command == "-test")
        {
            automatedTest();
        } 
        else if (command == "-auto")
        {
            std::string elements;
            std::cout << "Type number of substances needed: ";
            std::getline(std::cin, elements);
            SubstancesRelation substances(std::stoi(elements));

            substances.setup();
        }
        else if (command == "-manual")
        {
            std::string elements;
            std::cout << "Type number of substances needed: ";
            std::getline(std::cin, elements);
            SubstancesRelation substances(std::stoi(elements), false);

            substances.setup();
        }
    }
    else
    {
        std::cout << "At least one argument required!\n";
        return 1;
    }
    
    return 0;
}

void automatedTest()
{
    int elements = 4;
    SubstancesRelation substances(elements);

    std::cout << "\nForam gerados automaticamente " << elements << " elementos." << std::endl;
    std::cout << "Os nomes foram gerados Substance 0 ... Substance n - 1, com os identificadores 0 ... n-1" << std::endl;

    // SUBSTANCE 0
    substances.setRelation(0, 1, 2);
    substances.setRelation(0, 3, 3);
    // SUBSTANCE 1
    substances.setRelation(1, 1, 2);
    substances.setRelation(1, 2, 4);
    // SUBSTANCE 2
    substances.setRelation(2, 1, 2);
    substances.setRelation(2, 0, 4);
    // SUBSTANCE 3
    substances.setRelation(3, 2, 4);
    substances.setRelation(3, 1, 5);

    // ERROR (INDEX OUT OF RANGE)
    substances.setRelation(3, 4, 4);
    substances.removeRelation(3, 4);

    substances.printRelations();

    substances.iterateThroughStrongestRelations(0, 5);

    // Change chain
    substances.removeRelation(0, 3);

    substances.printRelations();

    substances.iterateThroughStrongestRelations(0, 5);
}