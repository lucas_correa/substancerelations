#include "WeightedGraph.h"
#include <iostream>
#include <vector>

struct Substance
{
    int identifier;
    std::string name;

    void setProperties(int id, std::string nm)
    {
        identifier = id;
        name = nm;
    }
};

class SubstancesRelation
{
protected:
    std::vector<Substance> substances;
    WeightedGraph substancesGraph;
    void printStrongestRelation(int substance, int strongestSubstance);
public:
    SubstancesRelation(int elements, bool autoname = true);
    void setRelation(int sub_a, int sub_b, float weight);
    void removeRelation(int sub_a, int sub_b);
    void iterateThroughStrongestRelations(int initialSubstance, int iterations);
    void printRelations();
    void setup();
    void shell();
};