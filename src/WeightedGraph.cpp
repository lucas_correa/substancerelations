#include "WeightedGraph.h"
#include <iostream>

WeightedGraph::WeightedGraph(int elements)
{   
    this->elements = elements;
}

void WeightedGraph::resize(int elements)
{
    this->elements = elements;
}

bool WeightedGraph::join(int a, int b, float weight)
{
    if (a >= 0 && b >= 0 && a < this->elements && b < this->elements)
    {
        this->adj[a][b].relation = true;
        this->adj[a][b].weight = weight;

        return true;
    }
    else
    {
        return false;
    }
}

bool WeightedGraph::remove(int a, int b)
{
    if (a >= 0 && b >= 0 && a < this->elements && b < this->elements)
    {
        this->adj[a][b].relation = false;
        this->adj[a][b].weight = 0.0;

        return true;
    }
    else
    {
        return false;
    }
}

int WeightedGraph::getStrongestNeighbour(int a)
{
    int strongestID = a;
    for (int i = 0; i < this->elements; i++)
    {
        if (this->adj[a][i].weight > adj[a][strongestID].weight)
        {
            strongestID = i;
        }
    }

    return strongestID;
}

void WeightedGraph::print() 
{ 
    std::cout << "\n****************************************************************************\n";
    std::cout << "GRAPH: \n";

    std::cout << "    ";

    for (int i = 0; i < this->elements; i++)
    {
        std::cout << i << "\t\t";
    }

    std::cout << std::endl;

    for (int i = 0; i < this->elements; i++)
    {
        std::cout << i << "  ";
        for (int j = 0; j < this->elements; j++)
        {
            std::cout << this->output[adj[i][j].relation] << " (w: " << adj[i][j].weight << ")  ";
        }
        std::cout << std::endl;
    }
    std::cout << "****************************************************************************\n";
}

float WeightedGraph::getWeight(int a, int b)
{
    if (a >= 0 && b >= 0 && a < this->elements && b < this->elements)
        return this->adj[a][b].weight;
    else
    {
        std::cout << "position " << a << " or " << b << " out of range!" << std::endl;
        return 0.0;
    }
}
