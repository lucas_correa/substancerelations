#include <string>

const int MAX_ELEMENTS = 100;

struct Arc
{
    float weight;
    bool relation;

    Arc()
    {
        weight = 0.0;
        relation = false;
    }
};

class WeightedGraph
{
protected:
    Arc adj[MAX_ELEMENTS][MAX_ELEMENTS];
    int elements;
    std::string output[2] = { "Nao", "Sim" };
public:
    WeightedGraph(int elements);
    void resize(int elements);
    bool join(int a, int b, float weight);
    bool remove(int a, int b);
    int getStrongestNeighbour(int a);
    void print();
    float getWeight(int a, int b);
};