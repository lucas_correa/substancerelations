#include "SubstancesRelation.h"

SubstancesRelation::SubstancesRelation(int elements, bool autoname) : substancesGraph(elements)
{
    this->substances.resize(elements);
    if (autoname)
    {
        for (int i = 0; i < substances.size(); i++)
        {
            std::string substanceName = "Substance " + std::to_string(i);
            substances[i].setProperties(i, substanceName);
        }
    }
    else
    {
        for (int i = 0; i < substances.size(); i++)
        {
            std::cout << "Type the substance name for ID:" << i << " : ";
            std::string substanceName;
            std::getline(std::cin, substanceName);
            substances[i].setProperties(i, substanceName);
        }
    }
    
}

void SubstancesRelation::setRelation(int sub_a, int sub_b, float weight)
{
    if(this->substancesGraph.join(sub_a, sub_b, weight))
    {
        std::cout << "\n****************************************************************************\n";
        std::cout << "Succesfully set a relation of the substances with ID:" << sub_a << " and ID:" << sub_b;
        std::cout << "\n****************************************************************************\n";
    }
    else
    {
        std::cout << "\n****************************************************************************\n";
        std::cout << "Not able to set a relation of the substances with ID:" << sub_a << " and ID:" << sub_b << std::endl;
        std::cout << "INDEX OUT OF RANGE";
        std::cout << "\n****************************************************************************\n";
    }
}

void SubstancesRelation::removeRelation(int sub_a, int sub_b)
{
    if(this->substancesGraph.remove(sub_a, sub_b))
    {
        std::cout << "\n****************************************************************************\n";
        std::cout << "Succesfully removed a relation of the substances with ID:" << sub_a << " and ID:" << sub_b;
        std::cout << "\n****************************************************************************\n";
    }
    else
    {
        std::cout << "\n****************************************************************************\n";
        std::cout << "Not able to remove a relation of the substances with ID:" << sub_a << " and ID:" << sub_b << std::endl;
        std::cout << "INDEX OUT OF RANGE";
        std::cout << "\n****************************************************************************\n";
    }
}

void SubstancesRelation::printStrongestRelation(int substance, int strongestSubstance)
{
    std::cout << this->substances[substance].name << " -> " << this->substances[strongestSubstance].name << std::endl;
}

void SubstancesRelation::iterateThroughStrongestRelations(int initialSubstance, int iterations)
{
    std::cout << "\n****************************************************************************\n";

    for (int i = 0; i < iterations; i++)
    {
        int strongestSubstance = this->substancesGraph.getStrongestNeighbour(initialSubstance);
        if(this->substancesGraph.getWeight(initialSubstance, strongestSubstance) == 0.0) { break; }
        this->printStrongestRelation(initialSubstance, strongestSubstance);
        initialSubstance = strongestSubstance;
    }

    std::cout << "****************************************************************************\n";
}

void SubstancesRelation::printRelations()
{
    this->substancesGraph.print();
}

void SubstancesRelation::setup()
{
    std::cout << "All elements were created, now you are free to manipulate them.\n";
    std::cout << "Type: 'set relation' to create a relation\n";
    std::cout << "Type: 'remove relation' to create a relation\n";
    std::cout << "Type: 'print relations' to print the graph of substances\n";
    std::cout << "Type: 'iterate relations' to iterate through strongest substances relation\n";
    std::cout << "Type: 'help' to show all commands\n";
    std::cout << "Type: 'exit' to stop the execution\n";

    this->shell();
}

void SubstancesRelation::shell()
{
    std::string operation;
    std::cout << ">> ";
    std::getline(std::cin, operation);

    if (operation == "set relation")
    {
        std::string sub_a, sub_b, weight;
        std::cout << "type the ID of the first substance: ";
        std::getline(std::cin, sub_a);
        std::cout << std::endl;

        std::cout << "type the ID of the second substance: ";
        std::getline(std::cin, sub_b);
        std::cout << std::endl;

        std::cout << "type the weight of the relation: ";
        std::getline(std::cin, weight);
        std::cout << std::endl;

        this->setRelation(std::stoi(sub_a), std::stoi(sub_b), std::stof(weight));
        this->shell();
    }
    else if (operation == "remove relation")
    {
        std::string sub_a, sub_b;
        std::cout << "type the ID of the first substance: ";
        std::getline(std::cin, sub_a);
        std::cout << std::endl;
        std::cout << "type the ID of the second substance: ";
        std::getline(std::cin, sub_b);
        std::cout << std::endl;

        this->removeRelation(std::stoi(sub_a), std::stoi(sub_b));
        this->shell();
    }
    else if (operation == "print relations")
    {
        this->printRelations();
        this->shell();
    }
    else if (operation == "iterate relations")
    {
        std::string initalSubstance, iterations;
        std::cout << "type the ID of the initial substance to start the iteration: ";
        std::getline(std::cin, initalSubstance);
        std::cout << std::endl;
        std::cout << "type the number of iterations needed: ";
        std::getline(std::cin, iterations);
        std::cout << std::endl;

        this->iterateThroughStrongestRelations(std::stoi(initalSubstance), std::stoi(iterations));
        this->shell();
    }
    else if (operation == "exit")
    {
        std::cout << "Execution killed!\n";
        exit(0);
    }
    else if (operation == "help")
    {
        setup();
    }
    else
    {
        std::cout << "Invalid command!\n";
        this->shell();
    }
}
