To execute the compiled file, run the following commands:

-- Windows --

cd [file-path]

substances [mode]

--- LINUX ---

cd [file-path]

./substances [mode]

There are three modes available:

-auto : it will generate automatic substances names and ids

-manual : it will generate automatic ids but it will ask for each substance name

-test : it will run an automated test 
